kind: component
parent: arch/Foobar/arch
title: Foobar copy-artifacts plugin
description: |
    The copy-artifacts plugin reads a system artifact to find the
    artifacts needed to re-create it and copies them to a location on
    another computer.

arch:
    kind: architecture
    title: Copy-artifacts mechanism
    description: |
        This plugin reads the metadata stored in a system artifact
        to determine which artifacts were needed to create it.

        The list-artifacts subcommand prints the paths to all the
        artifacts in the artifact cache.

        The copy-artifacts subcommand copies all the artifacts to
        the location specified.

        @startuml
        "/Foobar" -left-> "copy-artifacts"
        rsync -down-> "copy-artifacts"
        call_in_artifact_directory -right-> "copy-artifacts"
        @enduml

        @startuml
        "/Foobar" -left-> "list-artifacts"
        call_in_artifact_directory -right-> "list-artifacts"
        @enduml

        This uses the interfaces to rsync, the /Foobar directory, and
        the call_in_artifact_directory method.

    list-artifacts:
        kind: component
        title: Foobar list-artifacts
        description: |
            Given the metadata in the /Foobar directory of a system
            artifact, print the paths to all the artifacts in the
            artifact cache required to build that system.

            This will require mounting images and extracting tarballs
            to have access to the /Foobar directory.

        cli:
            kind: interface
            title: Foobar list-artifacts
            description: |
                Run as `Foobar list-artifacts $system_artifact`, where
                `system_artifact` is the path to the system artifact.

                It will print every artifact's path to Foobar's output.

                If the artifact cache is missing some of the artifacts,
                then this command will fail.

    copy-artifacts:
        kind: component
        title: Foobar copy-artifacts
        description: |
            Given the metadata in the /Foobar directory of a system
            artifact, find every artifact needed to build that system
            and copy them to the specified location.

        cli:
            kind: interface
            title: morpy copy-artifacts
            description: |
                Run as `Foobar copy-artifacts $system-artifact
                $destination`, where `system-artifact` is the path to
                the system artifact, and `destination` is how a
                destination is specified in rsync, i.e. `USER@HOST:PATH`

                Note: If this is used to copy artifacts to a cache
                server, either USER must be the cache user, or
                ownership will have to be changed later.
