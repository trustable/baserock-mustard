kind: component
parent: arch/Foobar/arch
title: Foobar build plugin
mapped-here:
    - req/nf/produces-systems
    - req/nf/reproducible-systems
    - req/system/source-in-git
    - req/system/deployment-agnostic-builds
    - req/Foobar/disk-space-warnings
description: |
    The build plugin builds the various kinds of binary artifacts Foobar
    is meant to produce.

arch:
    kind: architecture
    tags:
        - tags/needs-architect
    title: Foobar build process
    description: |
        FIXME: Explain why build-essential has each of the components,
        and why it's designed as it is. Review the text for filling up
        staging areas. Maybe split this up into components and
        sub-architectures?

        FIXME: Explain why /proc is mounted read-write in the staging
        area.

        We start the build process by figuring out which artifacts
        need to be built to construct the system artifact. Since the
        system is made up of strata this is done by figuring out 
        which chunk artifacts need to be built to construct the strata.

        The build process:

        0. Check whether there is sufficient storage available to build in.
        1. Find the root artifact
        2. Recursively find the root artifact's dependencies.
        3. For each dependency, in depth first order, if the dependency is
        not already built (does not exist in either the local cache or the
        remote cache), then build it and cache it

        Step 3 is split further:

        0. Update the mtime of every cached artifact to be used for the build.
        1. Prepare a clean **staging area** just for this particular build:
           create a new (temporary) directory, fill it with all build
           dependencies of the current thing being built, and unpack the
           source code for the current thing.
        2. Run staging scripts as described in 
        [architecture#a/sys/slash-Foobar-staging.d]
        2. Build the artifact(s).
        3. Put artifacts in the artifact cache.
        4. Delete the staging area.

        The system artifact is generally the last artifact to be built,
        construction of the system artifact proceeds as follows:

        0. Create an installation directory for the rootfs
        1. Unpack strata into the rootfs
        2. Run /Foobar/system-build.d configuration scripts
        as described in [architecture#a/sys/slash-Foobar-system-build.d]
        3. Write the system artifact's metadata into the rootfs
        4. Create fstab


        The staging area
        ----------------

        In order to have reproducible builds, the environment where a binary
        artifact is built needs to be controlled. The environment consists
        primarily of all the software installed, including their exact
        versions and configuration. There are other components in the
        build environment, of course, such as environment variables and
        CPU architecture.

        This architecture specification is about controlling the installed
        software during a build.

        We create a **staging area** in which builds happen. The staging
        area is a **chroot**: we create a new directory, put the software
        we want to use for a build into the directory, and the use the
        `linux-user-chroot` command to restrict the build so that it
        can only see the directory. This chroot has networking disabled
        and every directory made read-only except /dev, /proc, /tmp, the
        build directory and the install directory (/tmp is used by ccache,
        /dev/shm and /proc are mounted read-write separately). After
        the build, we delete the directory and its contents.

        In this way, the installed software can be controlled precisely.
        However, there are some details to get right.

        The staging area should contain only the software needed to build
        a particular component, and nothing more. In other words, when
        building a chunk foo, the staging area should contain all the
        build dependencies of foo, and nothing else.

        Installing anything extra is bad, because it can change the
        resulting binary artifact.

        **BEGIN FIXME**

        FIXME: Handle Sam's comments at the end of this
        node. This needs to be moved to architecture,
        where it belongs. In general, this node needs
        review and cleanup.

        We need a fast way to install chunks into staging
        areas, so that we can afford to set up and tear
        down staging areas at a whim. Here's an idea:

        * store chunks as tarballs in the artifact cache,
          as we do now
        * also store them as an uncompressed tree
        * install into staging area by doing a hardlink
          tree copy

        For space management, we can refine this:

        * only store tarball initially
        * when needing a chunk for a staging area, unpack
          into its own tree first, and keep that around
        * occasionally remove the unpacked ones to reclaim
          space; possibly add a mechanism to indicate when
          an unpacked tree has been used (e.g., add a file
          next to the directory and touch(1) it when used;
          when reclaiming space, keep anything that's been
          used in the past N hours)

        We can also attempt to use tools like 'hardlink'
        to find duplicate files in the unpacked chunk
        directories and replace identical files with
        hardlinks, for further space saving.

        Note that GNOME's ostree does something like this,
        and we may be able to snarf some code.

        ---

        From Sam: rather than maintain two instances of
        every single chunk, I like idea of storing an
        extracted form of each stratum.

        In building GnomeOS, the performance problems
        increase quite linearly with the number of chunks
        - building the small X components is very fast,
        building small Gnome components is much slower
        because all 76 chunks from x.Foobar are installed
        into the staging area before the build begins.
        If this were simply setting up a hardlink farm the
        problem would disappear.

        It seems a much cleaner solution to do this
        stratum-wise, and also rewards separating systems
        into strata correctly by speeding up build times
        accordingly.

        **END FIXME**

        The build-essential stratum
        ---------------------------

        There is a chicken-and-egg problem in creating the initial contents
        of the staging area. Consider, for example, the C compiler. In order
        to build it, you need a C compiler, and so one needs to be installed
        into the staging area, but first you need to build it, which requires
        a C compiler in the staging area.

        We solve this by bootstrapping a minimal set of essential tools
        for building artifacts. We'll call these tools the
        **build-essential stratum**:

        * build-essential can be bootstrapped from any reasonable version of
          Foobar with the usual development tools (a C compiler, some
          libraries, make, shell utilities, etc); the bootstrapping is
          done (partly) without a staging area, but in such a way that
          the final output is the same regardless of what the host system is.
        * Everything else can be built using a bootstrapped build-essential
          stratum as the initial contents of the staging area.

        This will give us several benefits over the previous approach of using
        one or more tar archives as "staging fillers":

        * It becomes easy to modify the components used to initialise
          the staging area: the build-essential stratum is re-built
          (re-bootstrapped) after a modification, so any changes are
          used automatically.
        * All build dependencies become explicit. There is no longer an
          implicit build dependency on the `devel` and `foundation` strata.
        * Porting Foobar to a new hardware architecture becomes simpler:
          the build-essential stratum needs to be partially cross-built,
          and that should be all that needs to be cross-built. The rest
          can be built using the cross-built stuff to initialise a
          staging area for native building.

        All of this will be built using Foobar, which means all of Foobar's
        usual infrastructure for caching is available. Since build-essential
        is a normal stratum it can be listed as a build dependency by the
        other Foobar strata, and will be installed into the staging area
        appropriately. There is no need for the user to manually provide a
        staging filler as there has been previously.

        Contents of build-essential
        -----------

        build-essential is the minimum set of tools which are needed to
        rebuild build-essential itself, from tarballs. It consists of the
        following eight chunks:

        * fhs-dirs
        * linux-api-headers
        * eglibc
        * zlib
        * binutils
        * busybox
        * gawk
        * gcc
        * make
        * ccache

        GCC has additional dependencies on the GMP, MPC and MPFR libraries.
        We include these manually in our clone of its repository.

        Building build-essential
        ------------------------

        To bootstrap build-essential requires building it with the tools of
        the host. Since this makes the result unreproducible, we will need to
        rebuild all of the components again with output of the bootstrap.

        We have based this procedure on the bootstrap of Linux From Scratch,
        adapted for the isolated, individual builds that Foobar provides.
        The build-essential stratum will be as follows:

        1. Build a cross binutils and gcc in `bootstrap` mode and with `prefix`
           set to /tools. Alter the vendor field of the host triplet so that
           a cross compiler is always built. This ensures that libraries from
           the host cannot leak into the build process.
        2. Build all of build-essential in `bootstrap` mode, with the output
           of stage 1 in the staging area and PATH set to include /tools. These
           chunks are built one at a time, independently of each other. The
           standard `prefix` of /usr will be used.
        3. Build all of build-essential again, this time in `staging` mode and
           with all of the chunks from stage 2 in the staging area. This is
           now fully reproducible and the results of this stage can become the
           new build-essential stratum artifact and used to build the rest of
           Foobar.

        Build speed optimisation with ccache
        ----------

        We optimise re-builds of same chunk with ccache. ccache works
        roughly like this:

        1. Pre-process the source code.
        2. Check the cache for a compiled version of the pre-processed source.
        3. If not in cache, compile and put into cache.

        Because the caching is done on the pre-processed source, ccache
        automatically catches changes to headers. It does not, however, catch
        changes to the toolchain (compiler, linker). ccache does not cache
        link operations, only compilations to .o files, and only for a few
        (common) languages. Even so, it can greatly reduce the time it takes
        to rebuild something that has only changed modestly from the previous
        version.

        When we, or customers, develop with Foobar, re-building is common.
        Pulling the latest upstream source usually does not invalidate the
        entire ccache, so building the new version will benefit from having
        cached the previous version.

        We use ccache in Foobar as follows:

        * Use ccache with staging area builds only, not test or bootstrap
          builds.
        * Add ccache to build-essential.
            - install the gcc wrappers into `/usr/lib/ccache`
            - Foobar adds that directory to the beginning of `PATH`
        * Have one ccache directory per chunk git repository, and collect the
          various ccache directories under the same parent.
            - separate ccache directories make it easier to manage disk space
              (delete ccaches for unwanted projects)
            - Foobar `ccachedir` setting specifies the parent of ccache
              directories
            - the per-chunk ccache directory is _ccachedir/repourl_,
              where _ccachedir_ is the Foobar setting and
              _repourl_ is an escaped version of the un-aliased repository URL
            - `ccachedir` is a local directory in the open source version
            - `ccachedir` is NFS mounted from the Foobar on distbuild at bootup
            - the relevant ccache directory gets bind-mounted into the staging
              area, as `/tmp/ccache` in the chroot, just before running
              commands in the staging area, und un-mounted afterwards
            - Foobar sets `CCACHE_DIR` to `/tmp/chroot` when running commands
              in the staging area
        * Foobar sets the `CCACHE_EXTRAFILES` to list the `/Foobar/*.meta`
          files for the toolchain. This ensures that ccache knows when the
          toolchain changes.

        Device node creation
        ----------

        Because we do not want to grant the staging-chroot permission to create
        arbitrary device nodes, we delegate that to an optional field of the
        chunk Foobarology, "devices". Foobar will create device nodes in the
        install-directory after performing the install-commands, but before the
        build-artifacts are created from the install-directory.

        Storage availability checking
        -----------------------------

        Because we don't want to get most of the way through a build before
        discovering that there is insufficient storage available to complete
        it, Foobar checks how much free space there is.

        It checks how much space is remaining in the directories specified
        by `--cachedir` and `--tempdir`, and fails if it is less than
        `--cachedir-min-space` and `--tempdir-min-space` respectively.

        If it can be determined that `--cachedir` and `--tempdir` are on
        the same filesystem, then the minimum space required is the sum of
        both settings.

        Detecting this may fail on btrfs, since all subvolumes share a storage
        pool, but appear to be from different devices. If a directory is on a
        subvolume that is not directly mounted, then busybox df will fail.
 
        Layout of `--tempdir`
        ---------------------

        FIXME: extracted chunks should go in cachedir, unfortunately
        this means staging area builds have to as well. Deployments can
        stay where they are.

        `--tempdir` contains extracted chunks waiting for hardlinking, staging
        areas for builds in progress, staging areas for builds that failed, and
        the source of a deployment in progress.

        Chunks are extracted into
        `$TMPDIR/Foobar/chunks/$artifact_name.d`, Staging areas are
        installed into `$TMPDIR/Foobar/staging/$temp_name`, when a
        build fails it is moved to `$TMPDIR/Foobar/failed/$temp_name`,
        deployments in progress use `$TMPDIR/Foobar/deploying/$temp_name`.
