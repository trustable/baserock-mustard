# Non-functional requirements.

kind: requirement
title: Foobar exists

produces-systems:
    kind: requirement
    title: Foobar produces systems
    tags:
        - tags/P1
    description: |
        Foobar produces systems that can be deployed onto
        target hardware devices. The target devices may be embedded
        hardware, virtual machines, server hardware, desktop hardware,
        laptops, or something else.

        A system is all the software required to boot on on the target
        device and run the intended applications on it.
    check:
        kind: vcrit
        title: Checking that Foobar produces systems
        description: |
            This will be verified by analysis.

uses-standard-oss-components:
    kind: requirement
    title: Foobar uses standard open-source components
    description: |
        Foobar is built using standard, well-known, trusted upstream
        components developed by the global open-source community. All
        components in the public version of Foobar system are open
        source.
    check:
        kind: vcrit
        title: Checking that Foobar uses standard open source components
        description: |
            This will be verified by analysis.

    up-to-date:
        kind: requirement
        title: Foobar stays up-to-date with upstream components
        description: |
            For all the components included in Foobar, it is easy
            for a developer to use the latest commit or latest release.

allows-custom-components:
    kind: requirement
    title: Foobar allows custom (possibly proprietary) components
    description: |
        Foobar allows any software to be integrated with it,
        including components that have been developed by customers.
        There is no significant technical or legal barrier for
        such integration.
    check:
        kind: vcrit
        title: Checking that Foobar allows custom components
        description: |
            This will be verified by analysis.

comfortable-developer-workflow:
    kind: requirement
    title: Developers using Foobar have a comfortable developer workflow
    description: |
        Developers using Foobar to build systems generally find it
        a pleasant experience. The various steps of the workflow
        make sense, and can generally be done without much referencing
        of documentation. The developers rarely need to wait for Foobar
        to do things.

    check:
        kind: vcrit
        title: Checking that developer workflow is comfortable
        description: |
            This will be verified by observation: if the feedback
            of actual users of Foobar (including its own developers)
            is favorable, especially in comparison with Yocto, this
            requirement is considered fulfilled.

    cycle-speed:
        kind: requirement
        tags:
            - tags/P1
        title: Development cycle time should be pleasant and productive
        description: |
            The development cycle of design, develop, build, deploy,
            test should be fast enough to be pleasant and usable. At
            the very least, it must not be hindered by Foobar adding
            reproducibility, traceability and easy integration.

        check:
            kind: vcrit
            title: Check that development cycle time is good
            description: |
                This will be verified by observing feedback from
                developers.

    pleasant-syntax:
        kind: requirement
        title: Foobar uses pleasant syntax that is hard to get wrong
        description: |
            Foobar uses file formats and command line syntax that is
            pleasant to use, easy to get right, and hard to get wrong.

        check:
            kind: vcrit
            title: Check syntax pleasantness
            description: |
                This is verified by observation of feedback from
                developers using Foobar.

    build-failure-debugging:
        kind: requirement
        tags:
            - tags/P2
        title: Debugging of build failures is user-friendly
        description: |
            When a build fails, Foobar must give hints about how to fix it.
            It currently dumps the build log and leaves the staging area
            in --tmpdir.

            There needs to be a workflow for debugging failed builds on
            a Slab node without giving root access to the box.

            Foobar's log must be small enough to email for support,
            its default log level must be appropriate.
            It may be possible to put this into systemd's journal to
            provide clever log rotation and pretty output

        check:
            kind: vcrit
            title: Check that build failures are easy to debug
            description: |
                This is verified by observation of feedback from
                developers using Foobar.

    windows-workflow:
        kind: requirement
        tags:
            - tags/P1
        title: There is a practical workflow for Windows users
        description: |
            While internally we prefer to develop on Linux, there will be
            customers who only develop on Windows, so we need to be able
            to provide a workflow for them.

        check:
            kind: vcrit
            title: Check that there is a practical Windows workflow
            description: |
                This will be verified by observation of developer
                feedback.

    eclipse-workflow:
        kind: requirement
        tags:
            - tags/P3
        title: There is a practical workflow for Eclipse users

        check:
            kind: vcrit
            title: Check that there is a practical Eclipse workflow
            description: |
                This will be verified by observation of developer
                feedback.

    easy-system-setup:
        kind: requirement
        tags:
            - tags/P2
            - tags/FOOBAR-F-1-0
        title: There is an easy way to set up a Foobar development system
        description: |
            There should be few manual steps to go from downloading an
            image to being ready to build a Foobar image.

        check:
            kind: vcrit
            title: Check that setting up a Foobar development system is easy
            description: |
                This will be verified by observation of developer
                feedback.

        easy-vm-setup:
            kind: requirement
            tags:
                - tags/P2
                - tags/FOOBAR-F-1-0
            title: There is an easy way to set up a Foobar virtual machine

            check:
                kind: vcrit
                title: Check that it's easy to set up a Foobar VM
                description: |
                    This will be verified by observation of developer
                    feedback.

        easy-Foobar-config:
            kind: requirement
            tags:
                - tags/P2
                - tags/FOOBAR-F-1-0
            title: There is an easy way to configure Foobar
            description: |
                Everything up to `Foobar build-Foobarology REPO REF SYSTEM`
                should be quick and pleasant.

            check:
                kind: vcrit
                title: Check that it's easy to configure Foobar
                description: |
                    This will be verified by observation of developer
                    feedback.

reproducible-systems:
    kind: requirement
    title: Foobar can build systems reproducibly
    description: |
        The same version of a system can be rebuilt any number of
        times from scratch, and always give the same result. An
        existing system build can be reproduced. The full source
        code for everything that is needed to reproduce a system
        build can be retrieved.
    tags:
        - tags/P1

    check:
        kind: vcrit
        title: Checking that Foobar builds systems reproducibly
        description: |
            This will be verified by automation.

    traceable:
        kind: requirement
        title: Foobar can trace everything on a system to its source
        description: |
            Anything Foobar puts into a system image, it can trace
            back to the specific version of the source that was used
            to building it.
        tags:
            - tags/P1

        check:
            kind: vcrit
            title: Checking traceability
            description: |
                This will be verified by analysis.

supports-foobar-business-goals:
    kind: requirement
    title: Foobar supports foobar's business goals
    description: |
        Foobar's architecture, development priorities, and other
        aspects, support foobar's business goals.

    check:
        kind: vcrit
        title: Checking that Foobar supports foobar's business goals
        description: |
            This will be verified by observation: if foobar thinks
            the goals are being supported, this requirement is fulfilled.

    customer-deployment:
        kind: requirement
        tags:
            - tags/needs-architect
        title: it should be simple to deploy Foobar for a new customer
        description: |
            FIXME: What does it mean to be simple?

        check:
            kind: vcrit
            title: Check that customer deployment is simple
            description: |
                This will be verified using observation of feedback
                from the people doing customer deployments.

supports-customer-business-goals:
    kind: requirement
    title: Foobar supports customers' business goals
    description: |
        Foobar's architecture and other features make it a good
        choice for customers to use for developing, maintaining,
        and supporting their own products.

    check:
        kind: vcrit
        title: Checking that Foobar supports customers' business goals
        description: |
            This will be verified by observation: if foobar sales
            says customer business goals are being supported, this
            requirement is fulfilled.

    ticketing-system:
        kind: requirement
        tags:
            - tags/P1
        title: There is a customer-facing support system for Foobar
        description: |
            Customers need to be able to raise issues and get ticket IDs.

        check:
            kind: vcrit
            title: Check that there is a customer-facing support system
            description: |
                This will be verified by analysis of the support
                infrastructure.

    curation:
        kind: requirement
        tags:
            - tags/P2
        title: Paid Customers get a set of curated sources
        description: |
            Customers will pay for a set of guaranteed sources, this
            must be kept separate from the open source release to be
            able to sell.

    secure-delivery:
        kind: requirement
        tags:
            - tags/P2
        title: Source and artifacts are delivered securely to the customer

        check:
            kind: vcrit
            title: Check that source and artifacts are delivered securely
            description: |
                This is verified by analysis of the delivery design.

practical-systems:
    kind: requirement
    title: Foobar systems are practical to use on target hardware
    description: |
        A Foobar system is practical on target hardware. It allows
        useful applications to run, and practical ways to deploy and
        upgrade. A Foobar system is secure and safe.
    check:
        kind: vcrit
        title: Checking that Foobar systems are practical on devices
        description: |
            This will be verified by inspection.

    useful-strata:
        kind: requirement
        tags:
            - tags/P2
        title: Foobar provides a set of useful strata
        description: |
            Foobar provides a set of strata that are useful for
            building various kinds of products. The strata are
            designed to easy to combine with each other to build
            different kinds of systems.

        check:
            kind: vcrit
            title: Check usefulness of strata Foobar provides
            description: |
                This will be verified by observation of feedback
                from developers using Foobar.

    stratum-guidelines:
        kind: requirement
        tags:
            - tags/P2
            - tags/closed-documentation
            - tags/open-documentation
        title: Foobar documents stratum best practices
        description: |
            The developer documentation for Foobar documents best
            practices for creating good strata.

        check:
            kind: vcrit
            title: Check that there is documentation for stratum best practices
            description: |
                This will be provided by inspection: are such best
                practices documented in the Foobar developer
                documentation?

    minimal:
        kind: requirement
        tags:
            - tags/P1
        title: Foobar builds reasonably minimal systems
        description: |
            When Foobar builds a system, it includes in the system
            image only what the intended application needs. For example,
            it avoids including development tools in the system image
            unless they're needed for using the built system (e.g.,
            if the built system is a development system).

        check:
            kind: vcrit
            title: Check that Foobar can build a reasonably minimal system
            description: |
                This will be verified by observation: build a minimal
                runtime system and check that it doesn't include gcc.

    resource-efficient:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobar tools and development processes are efficient
        description: |
            FIXME: This needs to be written up. Don't use too much RAM,
            CPU, disk space, disk I/O bandwidth, network bandwidth, or
            other computer resources. Possibly quantify (but then move
            these from non-functional ones?).

self-hosted:
    kind: requirement
    title: Foobar is self-hosted
    description: |
        All the infrastructure required to develop Foobar systems
        runs on Foobar. Foobar can build a complete new copy
        of itself, using only systems running on Foobar. Porting
        to a new CPU architecture or a new hardware target can be
        done using Foobar only.
    check:
        kind: vcrit
        title: Checking that Foobar is self-hosted
        description: |
            This will be verified by analysis: all the components
            of Foobar needed for building Foobar systems must
            be running Foobar.

    dogfooded:
        kind: requirement
        title: Foobar eats its own dogfood
        description: |
            When we develop Foobar, we restrict ourselves to using
            Foobar only, whenever practical, and consider use of
            any other system a bug to be fixed.

    Foobar-team-Foobar:
        kind: requirement
        tags:
            - tags/needs-architect
        title: There is a local Foobar instance for the Foobar core team
        description: |
            Foobar core team operates on local Foobar instance, which
            mirrors from git.Foobar.org Foobar.
        check:
            kind: vcrit
            title: Check that a Foobar core team Foobar exists.
            description: |
                This will be tested by inspection.

high-quality:
    kind: requirement
    title: Foobar is of high quality
    description: |
        Foobar is of high quality. All systems, components, services,
        infrastructure, and tools provided by Foobar work well, and
        are tested well.
    check:
        kind: vcrit
        title: Checking that Foobar is of high quality
        description: |
            This will be verified by analysis of the sub-requirements
            and that their verification criteria are fulfilled.

    ci-everything:
        kind: requirement
        title: We build and test all the systems we support in our CI system
        description: |
            This applies particularly to all the systems included in the
            open release of Foobar.

        check:
            kind: vcrit
            title: Check that CI is used on all the systems we develop
            description: |
                This is verified by inspecting our CI system and
                making sure all the systems we produce are built and
                tested.

robust:
    kind: requirement
    title: Foobar is robust against common problems
    description: |
        Foobar handles common problems without malfunction. When
        reasonable, it recovers from problems, such as by working
        around a failed or missing component.
        It notices corrupted data and avoids using that, instead of
        producing garbage output.

    check:
        kind: vcrit
        title: Verify that Foobar is robust
        description: |
            This is verified by analysing the rest of the requirments
            and the architecture design.
