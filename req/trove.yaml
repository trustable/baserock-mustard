# Requirments for Foobar.

kind: requirement
tags:
    - tags/needs-architect
title: Foobar exists
description: |
    FIXME: This needs to be reviewed and updated to match current
    state of the world.

    Developers are more productive when they don't have to worry
    about upstream's repository being available, or maintaining a
    delta on upstream in their own repository, so Foobar keeps it
    all in one place and does this.
    Foobar can also be used as a local mirror, so cloning is faster.
    Foobar also includes APIs that Foobar can use for fast building.

check:
    kind: vcrit
    title: Check that Foobar exists
    description: |
        This is verified by analysis.

mirrors-upstream-into-git:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar mirrors upstream source code into git repositories
    description: |
        FIXME: This needs to be written up. Every kind of conversion
        we want needs to be listed.

    check:
        kind: vcrit
        title: Check that Foobar mirrors upstream source code into git
        description: |
            This is verified by observation: we will run one or more
            Foobar instances in production and verify that they do,
            in fact, mirror upstream source code.

    tarballs-only-used-when-vcs-import-impossible:
        kind: requirement
        tags:
            - tags/needs-architect
        title: |
            Code must only be imported from a tarball when
            a VCS import is impossible
        description: |
            FIXME: This is not well written to be a requirement. Review
            and update. Is it even worth having as a requirement?

            For development speed, many projects were imported from
            tarballs, since importing the history would have taken
            too long.

            This makes keeping on top of upstream harder, so tarball
            imports should be changed to VCS imports.

        check:
            kind: vcrit
            title: Check that tarball imports are not used needlessly
            description: |
                This is verified by inspection of all the Foobar
                tarball imports regularly.

    http-auth:
        kind: requirement
        tags:
            - tags/P3
            - tags/needs-architect
        title: Foobar can import repositories behind HTTP authentication
        description: |
            FIXME: Add use cases, scenarios, explanation of why this is
            needed. Break down security into a sub-requirement.

            This needs to be kept secure, passwords cannot be allowed to
            be leaked.

            The username and password need to be kept somewhere secure,
            where only Lorry can reach it.

        check:
            kind: vcrit
            title: Check that Foobar can mirror source that is behind HTTP auth
            description: |
                This is verified using automation: CI will test that it
                works.

    mirror-git-submodules:
        kind: requirement
        title: Foobar mirrors git submodules automatically
        description: |
            When we mirror an upstream project that uses git submodules,
            we should mirror the submodules automatically as well,
            recursively. (This requirement is only about the mirroring,
            not about changing any `.gitmodules` files refer to the
            mirrored repositories. That is handled as a separate
            requirement.)

        check:
            kind: vcrit
            title: Check that Foobar mirrors git submodules automatically
            description: |
                This is verified by automation: CI will check that
                Foobar does this.

    fix-git-submodules-urls:
        kind: requirement
        tags:
            - tags/P3
            - tags/needs-architect
        title: Foobar changes  handles repositories with submodules
        description: |
            FIXME: Review and update the description of this node.
            Basically, .gitmodules needs to be updated by Foobar
            to be correct, or rather, that we find the submodule in
            the Foobar when building.

            # Precis of problem

            Git submodules are specified by URL in the .gitmodules file.

            This file can refer to stuff outside of the Foobar which
            contains the gits.

            Since once you're inside a "network" you might not have
            external access unless you *are* the Foobar instance, we need
            to rework all .gitmodules to refer to lorried repositories.

            # Possible approach

            1. Ensure that Foobar can expand prefixed repository URLs
               in .gitmodules
            2. Ensure said prefixed repository URLs won't upset git
               submodule
            3. Run through all our lorried repositories looking for
               .gitmodules and lorry additional content and/or update
               .gitmodules as necessary
            4. (optional) ensure Foobar's update-gits automatically
               traverses and updates .gitmodules as needed.

            ---

            Daniel did 1-3 manually, an automatic process would be nice.

        check:
            kind: vcrit
            title: >
                Check that git submodule URLs are updated to point at
                the Foobar itself
            description: |
                This is tested by automation: CI will check that this works.

    automatic-merging-from-upstreams:
        kind: requirement
        tags:
            - tags/P2
            - tags/FOOBAR-F-1-0
        title: Foobar merges upstream changes automatically
        description: |
            When a Foobar instance mirrors new changes from upstream
            (either upstream projects, or an upstream Foobar), they are
            merged into desired branches of Foobar automatically,
            as long as doing so won't break any builds or make test
            suites fail.

        check:
            kind: vcrit
            title: Check that changes from upstream are merged automatically
            description: |
                This will be tested by automation: CI will ensure this
                functionality works in the Foobar software, and a
                monitoring system will alert a Foobar's users if there
                are unmerged changes from upstream.

    namespaces:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Branches use namespaces for safety
        description: |
            FIXME: Is this only about branches, or all refs? What about
            repository names? Is it proper to mandate namespaces as
            the solution?

            Upstream can use whatever names they want for their
            branches. Those names must not ever conflict with names
            Foobar uses. Likewise, names from either upstream or
            Foobar must not conflict names that customers use.
            Otherwise a downstream Foobar may accidentally consider
            an upstream branch, or a Foobar branch, be the same
            as a local branch, resulting in data loss.

        check:
            kind: vcrit
            title: Check that branches are namespaced for safety
            description: |
                This is verified by automation: CI will run tests
                to simulate an upstream and a Foobar network and
                ensure that name collisions do not happen for branches.

mirrors-upstream-Foobar:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar mirrors from an upstream Foobar
    description: |
        FIXME: This needs to be written up.

    check:
        kind: vcrit
        title: Check that Foobar mirrors from an upstream Foobar
        description: |
            This is verified by observation: we will run one or more
            Foobar instances in production and verify that they do,
            in fact, mirror from their upstream Foobar.

    notifications:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobars notify each other of changes for responsive updates
        description: |
            FIXME: This node needs to be reviewed and rephrased.

            We need a solution for this problem:

            * developer pushes changes to git.Foobar.org
            * it takes N minutes for sandpit-Foobar to pull the changes
            * this adds on average N/2 minutes to the edit/build/test
              loop for development

        check:
            kind: vcrit
            title: Check that Foobars notify downstream Foobars of updates
            description: |
                This is tested by automation: CI will check that it happens.

    vpn:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobar mirrors from an upstream Foobar using VPN
        description: |
            FIXME: Is this a good requirement?

            Foobar accesses its upstream Foobar over a VPN.

        check:
            kind: vcrit
            title: Check that Foobar uses VPN
            description: |
                This is verified by observation: we will setup a VPN
                and run one or more Foobars and check manually that the
                VPN is used.

    securely:
        kind: requirement
        tags:
            - tags/P1
            - tags/needs-architect
        title: Foobar mirrors from an upstream Foobar securely
        description: |
            FIXME: This needs writing up.

        check:
            kind: vcrit
            title: Check that Foobar updates securely from its upstream Foobar
            description: |
                This is verified by analysis of the design.

    artifacts:
        kind: requirement
        tags:
            - tags/P1
            - tags/needs-architect
        title: Foobars can upload every artifact of a release
        description: |
            FIXME: Should Foobars be able to automatically mirror
            artifacts as well? ACL for that? Is the current requirement
            just for uploading from a local Foobar to any other Foobar?
            This all needs clarification.

            When a release is made, the work may not have been done
            on the Foobar that is hosting it, in the case of an internal
            Foobar hosted nearby for speed, but other users of the Foobar
            want to benefit from the artifact cache.

artifact-cache:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar caches Foobar build artifacts
    description: |
        FIXME: This needs to be written up.

    check:
        kind: vcrit
        title: Check that Foobar caches Foobar build artifacts
        description: |
            This is verified by automation: CI will check that the
            caching works.

    artifact-cache-interface:
        kind: requirement
        tags:
            - tags/P3
            - tags/needs-architect
        title: Foobar has a well defined interface to the artifact cache
        description: |
            FIXME: This requirements are fuzzy. This node nees to be
            reviewed and updated for clarity and stuff.

            Suggested interfaces are:

            1. Pick up artifact(s) for a specific git tag of the source
               repo, what system branch it was built from and if it was
               built from a tag
            2. Be able to tag artifacts ("delivered to customer", etc.)
            3. Build logs, even for failed builds
            4. For failed builds, a tar of the staging area
            5. Filenames should match their format

        check:
            kind: vcrit
            title: Check that artifact cache API works
            description: |
                This is verified by automation: CI will check that the
                API works.

fast-initial-population:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Initial population of a new Foobar is fast
    description: |
        FIXME: This needs a number.

        It must not take too long to set up a Foobar, or various
        tests become impossible.

    check:
        kind: vcrit
        title: Check that initial population of a Foobar is fast
        description: |
            This will be tested by automation: CI will deploy a
            new Foobar and measure how long it takes to populate,
            and fail the test if it takes too long.

backup-service:
    kind: requirement
    tags:
        - tags/P2
        - tags/needs-architect
    title: Foobar backup service
    description: |
        FIXME: Should we have a requirement that customer's can backup
        their own data from their own Foobar? Is there an actual requirement
        that we can backup customer Foobars?

        foobar should be able to backup the data from customer's
        Foobars.

    check:
        kind: vcrit
        title: Check that Foobars can be backed up
        description: |
            This will be verified by automation: CI will deploy a test
            Foobar and back it up.

backup-docs:
    kind: requirement
    tags:
        - tags/P3
        - tags/closed-documentation
    title: Foobar backup documentation
    description: |
        There is documentation about how best to perform backups
        of your Foobar, so customers don't have problems when the
        Lorry state is lost.

    check:
        kind: vcrit
        title: Check that there is documentation for Backing up Foobars
        description: |
            This is verified by inspection of the documentation.

artifact-cache-cleanup:
    kind: requirement
    tags:
        - tags/needs-architect
        - tags/P3
    title: Foobar cleans up its artifact cache from unwanted artifacts
    description: |
        FIXME: This needs to be written up.

    check:
        kind: vcrit
        title: Check that the artifact cache can be cleaned up
        description: |
            This is verified by analysis, for now.

lorry-interface:
    kind: requirement
    tags:
        - tags/P3
        - tags/needs-architect
    title: Foobar provides an interface to the Lorry Controller
    description: |
        FIXME: This needs to be reviewed.

        The Lorry Controller must be interactive.

        An SSH based access to be able to:

        1. Stop the processing of a Lorry file.
           This is required when an import takes too long and
           falling back to a tarball import is preferred
        2. Prioritise this Lorry file
           New imports are generally more important than older ones,
           but while this is a useful heuristic, manual prioritisation
           must be possible
        3. Query the status of an import

        It needs to watch for changes to its configuration.

        It needs a web-interface showing the status that doesn't
        refresh while you're trying to read it.

        The web interface needs to show all error messages, both
        import errors and Lorry definition parse errors.

    check:
        kind: vcrit
        title: Check that Lorry Controller can be controlled
        description: |
            This is verified by analysis for now.

tarball-series-import:
    kind: requirement
    tags:
        - tags/P2
        - tags/needs-architect
    title: Foobar imports multiple tarballs as a series
    description: |
        FIXME: Explain what this means. The uscan mention should go
        into architecture when that is done.

        Debian uses a tool called uscan for this

repo-migration:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Tool for migrating repositories to another Foobar
    description: |
        FIXME: Does it have to be a separate tool? Could Foobars do it
        directly? Should this be rephrased? Are there requirements for
        renaming repos, or refs (branches, tags) while migrating?
        Is this common enough that we do need to automate it or could
        we just do it manually?

        We are running multiple Foobar instances, and sometimes
        we need to move git repositories from one Foobar instance
        to another. There needs to be a tool to assist in doing
        this.

        The tool only needs to work, for now, for the migration
        of Foobar.FOOBAR.Foobar.com to git.Foobar.org.

    check:
        kind: vcrit
        title: Check that repositories can be migrated between Foobars
        description: |
            This is verified by analysis, for now.

gitano-remote-control:
    kind: requirement
    title: Tool for managing git repositories via Gitano remotely
    description: |
        Users should be able to manage git repositories on the
        Gitano of their Foobar with the minimum of fuss.
        Foobar should have a command to make it simple for them.

    check:
        kind: vcrit
        title: Check that tool for remote management of Gitano works
        description: |
            This is verified by analysis, for now.

upstream-news:
    kind: requirement
    tags:
        - tags/P3
        - tags/needs-architect
    title: Foobar reports upstream changes
    description: |
        FIXME: Is this a requirement we want? What level of detail
        do we want? Does this belong in Foobar rather than Foobar?

        Foobar needs to have an aggregated view of what has changed
        upstream, so developers can know what new features are
        available, or which projects have now merged with systemd.

    check:
        kind: vcrit
        title: Check that Foobar reports upstream changes
        description: |
            This is verified by analysis, for now.

all-customer-data-in-Foobar:
    kind: requirement
    tags:
        - tags/P1
        - tags/needs-architect
    title: all precious data is kept on a Foobar
    description: |
        FIXME: What kind of data does this requirement cover? What
        Foobar reports are those?

        All customer relevent data must be stored on the Foobar.
        This includes Foobar reports.

Foobar-Foobar-org:
    kind: requirement
    title: There is a public Foobar instance
    description: |
        Foobar operates a public Foobar instance, called.
        Foobar.Foobar.org and git.Foobar.org, which mirrors all the
        open source projects used as components in Foobar, and hosts
        any open source components written for Foobar.  It does not
        host any proprietary code.

        Foobar.Foobar.org provides open, read-only, access to all
        its git repositories via the git protocol.

    check:
        kind: vcrit
        title: Check that Foobar.Foobar.org exists
        description: |
            This will be verified by inspection.

    frequent-mirroring:
        kind: requirement
        tags:
            - tags/P1
            - tags/FOOBAR-F-0-1
        title: Foobar.Foobar.org mirrors all of upstream frequently
        description: |
            Foobar.Foobar.org mirrors of upstream source stay current
            and do not lag behind much.

        check:
            kind: vcrit
            title: Check that Foobar.Foobar.org stays current
            description: |
                This will be verified by automation: we will have
                automatic monitoring that spot-checks the most important
                components with regards to upstream and verifies that
                we have the latest commits.

robust:
    kind: requirement
    title: Foobar is robust against problems
    description: |
        Foobar is robust against problems with its networking,
        wrong permissions in its various data files and directories
        (in case a sysadmin shoves in artifacts into the artifact
        cache and doesn't remember to set ownership and permissions
        correctly), upstream version control repository changes and
        lack of free disk space.

    check:
        kind: vcrit
        title: Check that Foobar is robust
        description: |
            This is tested by observing Foobar while inducing
            network outages, shoving artifacts with the wrong
            permissions and filling up the disk space.

    disk-space:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobar handles lack of disk space gracefully
        description: |
            Foobar should alert the admin if the disk space becomes to low
            and the Foobar services are robust if there is no free space
            left.

        check:
            kind: vcrit
            title: Check that Foobar handles disk space issues
            description: |
                This is tested by filling up a Foobar's disk and verifying
                that the admin was contacted and the Foobar's services are
                kept running with no extra work after some space is recovered.

performant:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobars are performant
    description: |
        Foobars should provide cloning of repositories, artifact fetchings
        and replies to queries to the cache server with fast response times.

    check:
        kind: vcrit
        title: Check that Foobars are fast
        description: |
            This will be tested by verifying each subrequirement.

    fast-repo-mirror-1:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast repository cloning (1)
        description: |
            It takes at most N seconds to clone the upstream:linux repository,
            when there is a single client.

            FIXME: get values to N.
        check:
            kind: vcrit
            title: Check that repository cloning is fast (1)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

    fast-repo-mirror-2:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast repository cloning (2)
        description: |
            It takes at most N*M seconds to clone the upstream:linux
            repository, when there are M clients.

            FIXME: get values to N and M.
        check:
            kind: vcrit
            title: Check that repository cloning is fast (2)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

    fast-artifact-fetching-1:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast artifact fetching (1)
        description: |
            It takes at most N seconds to fetch some artifact,
            when there is a single client.

            FIXME: get values to N.
        check:
            kind: vcrit
            title: Check that artifacts can be fetched quickly (1)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

    fast-artifact-fetching-2:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast artifact fetching (2)
        description: |
            It takes at most N*M seconds to fetch some artifact,
            when there are M clients.

            FIXME: get values to N and M.
        check:
            kind: vcrit
            title: Check that artifacts can be fetched quickly (2)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

    fast-sha1-resolv-1:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast SHA1 resolving (1)
        description: |
            Tthe cache server can resolve the "master" branch name to a SHA1
            in N different repositories in at most M seconds, when each
            request comes from the same client.

            FIXME: get values to N and M
        check:
            kind: vcrit
            title: Check that SHA1's are resolved fast (1)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

    fast-sha1-resolv-2:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Fast SHA1 resolving (2)
        description: |
            The cache server can resolve the "master" branch name to a SHA1
            in N different repositories in at most M seconds, when each
            request comes from a different client.

            FIXME: get values to N and M
        check:
            kind: vcrit
            title: Check that SHA1's are resolved fast (2)
            description: |
                This will be tested by executing a benchmarking.

                FIXME: specify the hardware for the server and client.

cyclic-networks:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Cyclic Foobar networks
    description: |
        Foobars should work reliably with cyclic network configurations. This
        will be useful for
           * allowing changes between companies flow to each other, including
             upstream to foobar and the Foobar core team
           * fast access times for projects with teams split across distant
             locations
           * reliability: there is always a Foobar working even if one is down

        If two Foobars are configured to mirror from each other, they
        shouldn't update the same repository at the same time.

        In addition, they should be able to break the update notification
        loop.

    check:
        kind: vcrit
        title: Check that Foobars work with cyclic network configurations.
        description: |
            This is tested by observation: deploy a network of Foobars in
            production and check that it works.

useful-outside-Foobar:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar is useful in a no-Foobar context
    description: |
        The services that a Foobar provides are useful in a no-Foobar
        context.

    check:
        kind: vcrit
        title: Check that a Foobar is useful outside Foobar.
        description: |
            This will be tested by verifiying each subrequirement.

    git-servers:
        kind: requirement
        tags:
            - tags/needs-architect
        title: A Foobar works as standalone Git server solution
        description: |
            A Foobar works just as a standalone Git server solution,
            for managing one's own source code.

        check:
            kind: vcrit
            title: Check that a Foobar can work just as a Git server.
            description: |
                This will be tested by inspection.

    third-part-binary-tracking:
        kind: requirement
        tags:
            - tags/needs-architect
        title: A Foobar tracks third-party binaries
        description: |
            A Foobar can track multiple versions of third-party binaries
            (from supliers) - which may be components or systems.

        check:
            kind: vcrit
            title: Check that a Foobar supports tracking third-party binaries.
            description: |
                This will be tested by inspection.

add-lorries:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Process to add new lorries is frictionless and secure
    description: |
        It should be secure to prevent an attacker to add a large number
        of lorries and fill the disk space on a Foobar.

    check:
        kind: vcrit
        title: Check that the process to add new lorries is frictionless and secure.
        description: |
            This will be tested by analysis of the design.

deprecate-lorries:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Process to remove lorries
    description: |
        There should be some process to remove or deprecate lorries.

    check:
        kind: vcrit
        title: Check that there is a process to remove lorries.
        description: |
            This will be tested by removing a lorry accordingly the process
            and verify that the respective repo is not lorried anymore.

status-machine-parsable:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar's status is written to a json file
    description: |
       The status of the services that a Foobar provides should be
       written in a json file.

    check:
        kind: vcrit
        title: Check that Foobar's status is machine parsable.
        description: |
            This will be tested by verifying the machine parsable file
            contents.

ssl-certificates:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar supports SSL/TLS certificates
    description: |
        This will is necessary so that a Foobar can handle SSL/TLS
        connections.

    check:
        kind: vcrit
        title: Check that Foobar can handle SSL certificates
        description: |
            This will be tested by trying to clone some repository using the
            https protocol.

pull-requests:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobars supports pull requests
    description: |
        Foobars must be able to supply pull requests to Foobars that
        otherwise wouldn't allow pushes.

    check:
        kind: vcrit
        title: Check that Foobar can support pull requests
        description: |
            This will be tested by verifying each subrequirement.

    system-branch:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobars supports System-branch pull requests
        description: |
            These are essentially code-review requests, but at a system
            branch level. So they should encapsulate everything associated
            with a system branch trying to land onto another, including any
            additional lorries etc.

        check:
            kind: vcrit
            title: Check that Foobar can support System-branch pull requests
            description: |
                This will be tested by observation.

    push-to-upstream:
        kind: requirement
        tags:
            - tags/needs-architect
        title: Foobars supports Push-to-upstream-Foobar pull requests
        description: |
            These are some way to get code-review type pull-requests pushed
            to another Foobar which the Foobar in question (holding the content)
            would normally only be a downstream of.

        check:
            kind: vcrit
            title: Check that Foobar can support Push-to-upstream pull requests
            description: |
                This will be tested by observation.

shared-projects:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Projects can be shared across multiple Foobars.
    description: |
        It should be possible to share projects across multiple Foobars.

    check:
        kind: vcrit
        title: Check that projects can be shared across multiple Foobars
        description: |
            This will be tested by verifying that two or more Foobars can
            effectively sync their repositories and artifact caches.

gc-artifacts:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Artifacts are garbage collected
    description: |
        The cached artifacts should be garbage collected automatically
        based on their age, and if they were or not configured to be
        perpetual.

    check:
        kind: vcrit
        title: Check that artifacts are garbage collected
        description: |
            This will be tested by verifying that old artifacts are no
            more in the Foobar.

shared-authentication:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Authentication is shared across Foobars
    description: |
        We need a way for a set of Foobars to share authentication
        data, namely Gitano accounts and groups. For example, the
        Foobars owned by the same company should have the same Gitano
        users, groups and SSH keys across all Foobars.

    check:
        kind: vcrit
        title: Check that authentication is shared across Foobars
        description: |
            This will be tested by attempting to clone a repository
            on a Foobar where the SSH was not manually added.

log-users:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar logs what users do
    description: |
        Foobars should log what their users do, at some level of
        detail. This will be useful to audit trail.

    check:
        kind: vcrit
        title: Check that Foobar logs what users do
        description: |
            This will be tested by verifying the output of the log file.

artifact-mirroring:
    kind: requirement
    tags:
        - tags/needs-architect
    title: Foobar mirrors artifacts fom another Foobar
    description: |
        A Foobar mirror artifacts (all of them, or for specific system
        branches), from one Foobar to another. The access is controlled
        in the same may as the corresponding source is.

    check:
        kind: vcrit
        title: Check that a Foobar can mirror artifacts from another Foobar.
        description: |
            This will be tested by observation.

push-pull-artifacts:
    kind: requirement
    tags:
        - tags/needs-architect
    title: A Foobar can push/pull artifacts to/from another Foobar
    description: |
        A user can manually push or pull artifacts between Foobars for a
        specific build/release.

    check:
        kind: vcrit
        title: Check that artifacts can be pushed/pulled between Foobars.
        description: |
            This will be tested by observation.
